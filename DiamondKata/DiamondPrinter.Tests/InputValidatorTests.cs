﻿using DiamondPrinter.Validation;
using FluentAssertions;
using Xunit;

namespace DiamondPrinter.Tests;

public class InputValidatorTests
{
    private readonly InputValidator _validator = new();
    
    [Theory]
    [MemberData(nameof(InvalidArrayData))]
    public void HandlesInvalidArray(string[] args)
    {
        // Act
        var result = _validator.Validate(args);
        
        // Assert
        result.IsSuccessful.Should().BeFalse("Invalid data was provided");
        result.Error.Should().Be(ValidationStatus.InvalidData().Error, "Error message should be fulfilled");
    }
    
    [Theory]
    [MemberData(nameof(InvalidLettersData))]
    public void HandlesInvalidLetters(string[] args)
    {
        // Act
        var result = _validator.Validate(args);
        
        // Assert
        result.IsSuccessful.Should().BeFalse("Invalid data was provided");
        result.Error.Should().Be(ValidationStatus.InvalidLetter().Error, "Error message should be fulfilled");
    }

    public static IEnumerable<object[]> InvalidArrayData()
    {
        yield return new[] {(object)null};
        yield return new[] { Array.Empty<string>() };
    }
    
    public static IEnumerable<object[]> InvalidLettersData()
    {
        yield return new[] { new[] {"-"} };
        yield return new[] { new[] {"/"} };
        yield return new[] { new[] {" "} };
        yield return new[] { new[] {"0"} };
        yield return new[] { new[] {"a"} };
        yield return new[] { new[] {"ą"} };
        yield return new[] { new[] {"Ü"} };
    }
}