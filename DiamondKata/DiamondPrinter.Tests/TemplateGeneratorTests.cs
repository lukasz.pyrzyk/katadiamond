﻿using FluentAssertions;
using Xunit;

namespace DiamondPrinter.Tests;

public class TemplateGeneratorTests
{
    private readonly TemplateGenerator _generator = new();
    
    [Fact]
    public void GetsSingleLineForA()
    {
        // Arrange
        var letter = 'A';
        
        // Act
        var lines = _generator.GetLines(letter);
        
        // Assert
        lines.Should().HaveCount(1);
        lines[0].Should().Be("A");
    }
    
    [Fact]
    public void GetsThreeLinesForB()
    {
        // Arrange
        var letter = 'B';
        
        // Act
        var lines = _generator.GetLines(letter);
        
        // Assert
        lines.Should().HaveCount(3);
        lines[0].Should().Be(" A ");
        lines[1].Should().Be("B B");
        lines[2].Should().Be(" A ");
    }
    
    [Fact]
    public void GetsThreeLinesForC()
    {
        // Arrange
        var letter = 'C';
        
        // Act
        var lines = _generator.GetLines(letter);
        
        // Assert
        lines.Should().HaveCount(5);
        lines[0].Should().Be("  A  ");
        lines[1].Should().Be(" B B ");
        lines[2].Should().Be("C   C");
        lines[3].Should().Be(" B B ");
        lines[4].Should().Be("  A  ");
    }
}