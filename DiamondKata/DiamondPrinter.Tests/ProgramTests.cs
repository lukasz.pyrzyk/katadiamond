﻿using System.Text;
using FluentAssertions;
using Xunit;

namespace DiamondPrinter.Tests;

public class ProgramTests
{
    [Theory]
    [InlineData(" ")]
    [InlineData("-")]
    [InlineData("")]
    [InlineData("ą")]
    [InlineData("aa")]
    public void InvalidInputIsValidated(string input)
    {
        // Arrange
        var output = new InMemoryTextWriter();
        Program.Logger = output;
        var args = new[] { input };
        
        // Act
        Program.Main(args);
        
        // Assert
        output.Lines.Should().ContainSingle(x => x.StartsWith("Invalid data"));
    }
    
    [Fact]
    public void PrintsDiamondForA()
    {
        // Arrange
        var output = new InMemoryTextWriter();
        Program.Logger = output;
        
        // Act
        Program.Main(new[] {"A"});
        
        // Assert
        var lines = output.Lines;
        lines.Should().HaveCount(1);
        lines[0].Should().Be("A");
    }
    
    [Fact]
    public void PrintsDiamondForB()
    {
        // Arrange
        var output = new InMemoryTextWriter();
        Program.Logger = output;
        
        // Act
        Program.Main(new[] {"B"});
        
        // Assert
        var lines = output.Lines;
        lines.Should().HaveCount(3);
        lines[0].Should().Be(" A ");
        lines[1].Should().Be("B B");
        lines[2].Should().Be(" A ");
    }
    
    [Fact]
    public void PrintsDiamondForC()
    {
        // Arrange
        var output = new InMemoryTextWriter();
        Program.Logger = output;
        
        // Act
        Program.Main(new[] {"C"});
        
        // Assert
        var lines = output.Lines;
        lines.Should().HaveCount(5);
        lines[0].Should().Be("  A  ");
        lines[1].Should().Be(" B B ");
        lines[2].Should().Be("C   C");
        lines[3].Should().Be(" B B ");
        lines[4].Should().Be("  A  ");
    }
    
    class InMemoryTextWriter : TextWriter
    {
        public List<string> Lines { get; } = new();

        public override void WriteLine(string value)
        {
            Lines.Add(value);
            base.WriteLine(value);
        }

        public override Encoding Encoding { get; } = Encoding.Default;
    }
}