﻿namespace DiamondPrinter;

public class TemplateGenerator
{
    public string[] GetLines(char letter)
    {
        var positionInAlphabet = letter - 'A';
        var dimension = 2 * positionInAlphabet + 1;
        var lines = new string[dimension];
        
        for (var i = 0; i < dimension; i++)
        {
            var distanceFromCenter = Math.Abs(positionInAlphabet - i);
            var currentLetter = (char)('A' + positionInAlphabet - distanceFromCenter);
            
            var line = new char[dimension];
            Array.Fill(line, ' ');

            var leftPosition = distanceFromCenter;
            line[leftPosition] = currentLetter;
            
            if (currentLetter != 'A') {
                var rightPosition = dimension - distanceFromCenter - 1;
                line[rightPosition] = currentLetter;
            }
            
            lines[i] = new(line);
        }

        return lines;
    }
}