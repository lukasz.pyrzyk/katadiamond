﻿namespace DiamondPrinter.Validation;

public class InputValidator
{
    public ValidationStatus Validate(string[] args)
    {
        if (args is null || args.Length != 1) 
            return ValidationStatus.InvalidData();

        var argument = args[0];
        if (!char.TryParse(argument, out var parsedLetter) || !char.IsAsciiLetterUpper(parsedLetter))
            return ValidationStatus.InvalidLetter();

        return ValidationStatus.Success(parsedLetter);
    }
}