﻿namespace DiamondPrinter.Validation;

public class ValidationStatus
{
    private ValidationStatus(char letter) => Letter = letter;
    
    private ValidationStatus(string error) => Error = error;
    
    public char Letter { get; }

    public string Error { get; }

    public bool IsSuccessful => string.IsNullOrWhiteSpace(Error);

    public static ValidationStatus Success(char letter) => new(letter);
    
    public static ValidationStatus InvalidData() => new("Input missing, please provider a valid letter from A to Z");
    
    public static ValidationStatus InvalidLetter() => new("Invalid data, given letter is not in range from A to Z");
}