﻿using DiamondPrinter.Validation;

namespace DiamondPrinter;

public static class Program
{
    public static TextWriter Logger { get; set; } = Console.Out;

    public static void Main(string[] args)
    {
        var result = new InputValidator().Validate(args);
        if (!result.IsSuccessful)
        {
            Logger.WriteLine(result.Error);
            return;
        }

        var templateGenerator = new TemplateGenerator();
        var lines = templateGenerator.GetLines(result.Letter);
        foreach (var line in lines)
        {
            Logger.WriteLine(line);
        }
    }
}